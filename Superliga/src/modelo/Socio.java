package modelo;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Socio implements Comparable <Socio> {
	private String nombre;
	private int edad;
	private String equipo;
	private String estadoCivil;
	private String nivelDeEstudios;

	public Socio(String nombre, int edad, String equipo, String estadoCivil, String nivelDeEstudios){
		this.nombre=nombre;
		this.edad=edad;
		this.equipo=equipo;
		this.estadoCivil=estadoCivil;
		this.nivelDeEstudios=nivelDeEstudios;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEquipo() {
		return equipo;
	}

	public int getEdad() {
		return edad;
	}

	public String getNivelDeEstudios() {
		return nivelDeEstudios;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	@Override
	public int compareTo(Socio otroSocio) {
		if(this.edad<otroSocio.edad){
			return -1;
		}
		else if (this.edad>otroSocio.edad){
			return 1;
		}
		return 0;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return "Socio [nombre=" + nombre + ", edad=" + edad + ", equipo=" + equipo + ", estadoCivil=" + estadoCivil
				+ ", nivelDeEstudios=" + nivelDeEstudios + "]";
	}
	
	
}