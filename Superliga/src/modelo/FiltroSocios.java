package modelo;

import java.util.ArrayList;
import modelo.Equipo;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FiltroSocios {
	private ArrayList<Socio> listadoSocios;

	public FiltroSocios(ArrayList<Socio> sociosCargados){
		listadoSocios= sociosCargados;
	}

	public int cantidadTotalSocios(){
		return listadoSocios.size();
	}

	public OptionalDouble promedioEdadSocios(String club){
		return listadoSocios.stream().filter(s -> s.getEquipo().equals(club)).mapToInt(p -> p.getEdad()).average();
	}

	public ArrayList<Socio> filtrarCienPersonas(String nivelEstudio, String estadoCivil){
		Stream <Socio> filtro= listadoSocios.stream()
				.filter(s-> (s.getNivelDeEstudios().equals(nivelEstudio) && s.getEstadoCivil().equals(estadoCivil)))
				.limit(100);
		return devolverArray(filtro.sorted());	
	}

	private <T> ArrayList<T> devolverArray(Stream<T> stream) {
		return stream
				.collect(Collectors
						.toCollection(ArrayList::new));
	}

	public ArrayList<String> nombresMasComunes(String club,int cantAMostrar){
		HashMap <String, Integer> nombresSocios= new HashMap<String,Integer>();
		listadoSocios.stream().filter(s-> s.getEquipo().equals(club))
		.forEach(s->{
			if(!nombresSocios.containsKey(s.getNombre())){
				nombresSocios.put(s.getNombre(), 1);
			}
			else{
				nombresSocios.put(s.getNombre(), nombresSocios.get(s.getNombre())+1);
			}});
		return ordenarNombres(nombresSocios, cantAMostrar);
	}

	public ArrayList<String> ordenarCantSocios(){
		HashMap <String, Integer> clubes= new HashMap<String,Integer>();
		listadoSocios.stream().forEach(s->{if(!clubes.containsKey(s.getEquipo())){
			clubes.put(s.getEquipo(), 1);
		}
		else{
			clubes.put(s.getEquipo(), clubes.get(s.getEquipo())+1);
		}});

		return ordenarNombres(clubes,clubes.size());
	}

	private ArrayList<String> ordenarNombres(HashMap<String, Integer> nombresSocios, int cantAMostrar) {
		return devolverArray(nombresSocios.entrySet()
				.stream()
				.sorted((Map.Entry.<String, Integer>comparingByValue()
						.reversed())).limit(cantAMostrar).map(entry-> entry.getKey()));
	}

	public OptionalInt buscarMenorEdad(String club){
		return listadoSocios.stream().filter(s -> s.getEquipo().equals(club)).mapToInt(p -> p.getEdad()).min();
	}

	public OptionalInt buscarMayorEdad(String club){
		return listadoSocios.stream().filter(s -> s.getEquipo().equals(club)).mapToInt(p -> p.getEdad()).max();
	}

	public ArrayList<Equipo> asignarEquipos(){
		ArrayList<Equipo> listadoEquipos= new ArrayList<Equipo>();
		ordenarCantSocios().stream().forEach(s->{listadoEquipos.add(new Equipo(s,promedioEdadSocios(s).getAsDouble(), buscarMayorEdad(s).getAsInt(), buscarMenorEdad(s).getAsInt()));});
		return listadoEquipos;
	}

	public long cantidadDeSocios(String equipo){
		return listadoSocios.stream().filter(s-> s.getEquipo().equals(equipo)).count();
	}
}
