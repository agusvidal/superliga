package modelo;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Equipo {
	private String nombre;
	private double promedioEdadSocios;
	private int edadPersonaMayor; 
	private int edadPersonaMenor;

	public Equipo(String nombre, double d, int edadPersonaMayor, int  edadPersonaMenor){
		this.nombre=nombre;
		this.promedioEdadSocios=d;
		this.edadPersonaMayor=edadPersonaMayor;
		this.edadPersonaMenor=edadPersonaMenor;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPromedioEdadSocios() {
		return promedioEdadSocios;
	}

	public int getEdadPersonaMayor() {
		return edadPersonaMayor;
	}

	public int getEdadPersonaMenor() {
		return edadPersonaMenor;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}

