package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;


import modelo.FiltroSocios;

public class DatosAMostrar extends JDialog {
	private static final long serialVersionUID = 5780626924982813599L;
	private final JPanel contentPanel = new JPanel();
	private JTextArea datos;
	private FiltroSocios filtro;

	public DatosAMostrar(FiltroSocios filtro) {
		this.filtro=filtro;
		setBounds(100, 100, 500, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);	
		}
		datos= new JTextArea("");
		datos.setLineWrap(true);
		datos.setOpaque(false);
		datos.setWrapStyleWord(true);
		datos.setForeground(Color.BLACK);
		datos.setBounds(10, 120, 500, 800);
		datos.setFont(new Font("Louis George Caf�", Font.BOLD, 40));
		datos.setEditable(false);
		getContentPane().add(datos);
		crearFondo();
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setOpaque(false);
		scrollPane.setForeground(Color.RED);
		scrollPane.setBounds(50,20, 480, 600);
		scrollPane.isOpaque();
		getContentPane().add(scrollPane);
	}

	public void subirDatos(int opcion, String numero){
		switch(opcion){
		case 1:{
			datos.setText("La cantidad de personas registradas es\n "+numero);
			break;
		}	
		case 2:{
			datos.setText("El promedio de edad de los socios de Racing es:\n"+ numero);
			break;}

		case 4:{
			datos.setText("Los nombres m�s comunes son\n "+filtro.nombresMasComunes("River",5).toString());
			break;}
		}
	}
	private void crearFondo() {
		JLabel fondo = new JLabel();
		fondo.setBounds(0, 10, 500, 500);
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/resources/datos.jpg")));
		getContentPane().add(fondo);
		{
		}
	}
}
