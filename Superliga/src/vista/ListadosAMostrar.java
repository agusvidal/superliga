package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.FiltroSocios;
import modelo.Socio;

public class ListadosAMostrar extends JDialog {

	private static final long serialVersionUID = 2403471032630987684L;

	private final JPanel contentPanel = new JPanel();
	private static JTable table;
	private static FiltroSocios filtro;

	public ListadosAMostrar(FiltroSocios filtro) {
		ListadosAMostrar.filtro= filtro;
		setBounds(100, 100, 600, 650);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setOpaque(false);
		scrollPane.setForeground(Color.RED);
		scrollPane.setBounds(50,20, 480, 500);
		getContentPane().add(scrollPane);
		setLocationRelativeTo(null);	
		setResizable(false);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		crearTabla(scrollPane);
		this.setVisible(false);
	}

	private void crearTabla(JScrollPane scrollPane) {
		table = new JTable();
		table.setBorder(new LineBorder(new Color(158, 231, 132 )));
		table.setGridColor(new Color(158, 231, 132 ));
		scrollPane.setViewportView(table);
		table.setFont(new Font("Calibri", Font.BOLD, 17));
		table.getTableHeader().setFont(new Font("Calibri", Font.BOLD, 18));
		table.setBackground(Color.white);
		table.setForeground(Color.gray);
		table.getTableHeader().setBackground(new Color(158, 231, 132 ));
		table.getTableHeader().setForeground(Color.white);
		table.getTableHeader().setReorderingAllowed(false);
	}

	public static void cargarTabla(int largo, int ancho) {
		Object [][]datos = new Object[largo][ancho];
		if(ancho==3){
			cargarMatriz(datos);

			table.setModel(
					new DefaultTableModel(datos, new String[] {"NOMBRE", "EDAD", "EQUIPO"}) 
					{
						private static final long serialVersionUID = -1477113217095744696L;
						@SuppressWarnings({ "rawtypes" })
						Class[] columnTypes = new Class[] {
								Object.class, Integer.class, Object.class
						};
						@SuppressWarnings({ "rawtypes", "unchecked" })
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
					});

		}
		else{
			cargarMatrizEquipos(datos);
			table.setModel(
					new DefaultTableModel(datos, new String[] {"EQUIPO", "SOCIOS","PROMEDIO EDAD", "MAYOR EDAD", "MENOR EDAD"}) 
					{
						private static final long serialVersionUID = -9115382916063036409L;
						@SuppressWarnings("rawtypes")
						Class[] columnTypes = new Class[] {
								Object.class, Long.class, Double.class, Integer.class, Integer.class
						};
						@SuppressWarnings({ "rawtypes", "unchecked" })
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
					});	
		}

		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
	}

	private static void cargarMatrizEquipos(Object[][] datos) {
		ArrayList<String> equipos= filtro.ordenarCantSocios();
		for(int i=0; i<equipos.size();i++){
			String club=equipos.get(i);
			datos[i][0] = club;
			datos[i][1]=filtro.cantidadDeSocios(club);
			datos[i][2]= filtro.promedioEdadSocios(club).getAsDouble();
			datos[i][3]= filtro.buscarMayorEdad(club).getAsInt();
			datos[i][4]= filtro.buscarMenorEdad(club).getAsInt();	
		}
	}

	private static void cargarMatriz(Object [][]datos) {
		ArrayList<Socio> socios=filtro.filtrarCienPersonas("Universitario", "Casado");
		for(int i=0; i<socios.size();i++){
			datos[i][0] = socios.get(i).getNombre();
			datos[i][1]= socios.get(i).getEdad();
			datos[i][2]= socios.get(i).getEquipo();	
		}
	}
}
