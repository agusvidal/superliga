package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import modelo.FiltroSocios;
import utilitarios.LecturaCSV;
import javax.swing.JButton;

public class InterfazSuperliga {
	private LecturaCSV lector;
	private FiltroSocios filtro;
	private JFrame frame;

	public InterfazSuperliga() throws IOException {
		super();
		crearEntorno();
	}

	private void crearEntorno() throws IOException {
		lector= new LecturaCSV();
		filtro= new FiltroSocios(lector.obtenerSocios());
		setFrame(new JFrame());
		getFrame().setResizable(false);
		getFrame().setBounds(100, 100, 450, 500);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		getFrame().setLocationRelativeTo(null);
		crearBotones();
		crearCarteles();
		crearFondo();
	}

	private void crearBotones() {
		JButton boton1 = new JButton("OPCION 1");
		boton1.setBounds(308, 133, 97, 25);
		getFrame().getContentPane().add(boton1);
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DatosAMostrar dialog= new DatosAMostrar(filtro);
				dialog.subirDatos(1,String.valueOf(filtro.cantidadTotalSocios()));
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);
			}
		});

		JButton boton2 = new JButton("OPCION 2");
		boton2.setBounds(308, 183, 97, 25);
		getFrame().getContentPane().add(boton2);
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DatosAMostrar dialog= new DatosAMostrar(filtro);
				dialog.subirDatos(2,String.valueOf(filtro.promedioEdadSocios("Racing").getAsDouble()));
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);		
			}
		});

		JButton boton3 = new JButton("OPCION 3");
		boton3.setBounds(308, 263, 97, 25);
		getFrame().getContentPane().add(boton3);
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListadosAMostrar dialog= new ListadosAMostrar(filtro);
				ListadosAMostrar.cargarTabla(filtro.filtrarCienPersonas("Universitario", "Casado").size(), 3);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);
			}
		});

		JButton boton4 = new JButton("OPCION 4");
		boton4.setBounds(308, 351, 97, 25);
		getFrame().getContentPane().add(boton4);
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DatosAMostrar dialog= new DatosAMostrar(filtro);
				dialog.subirDatos(4, "River");
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);
			}
		});
		JButton boton5 = new JButton("OPCION 5");
		boton5.setBounds(308, 414, 97, 25);
		getFrame().getContentPane().add(boton5);	
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListadosAMostrar dialog= new ListadosAMostrar(filtro);
				ListadosAMostrar.cargarTabla(filtro.ordenarCantSocios().size(), 5);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);
			}
		});
	}

	private void crearCarteles() {
		JLabel titulo = new JLabel("SUPERLIGA");
		titulo.setForeground(Color.DARK_GRAY);
		titulo.setBackground(Color.WHITE);
		titulo.setFont(new Font("Segoe UI Black", Font.BOLD, 50));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBounds(58, 37, 318, 83);
		getFrame().getContentPane().add(titulo);
		JLabel opcion1 = new JLabel("Cantidad total de personas registradas.");
		opcion1.setForeground(Color.WHITE);
		opcion1.setBackground(Color.WHITE);
		opcion1.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		opcion1.setHorizontalAlignment(SwingConstants.CENTER);
		opcion1.setBounds(12, 127, 284, 34);
		getFrame().getContentPane().add(opcion1);
		JTextArea opcion2 = new JTextArea("El promedio de edad de los socios de Racing.");
		opcion2.setLineWrap(true);
		opcion2.setOpaque(false);
		opcion2.setWrapStyleWord(true);
		opcion2.setForeground(Color.WHITE);
		opcion2.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		opcion2.setBounds(12, 174, 300, 60);
		getFrame().getContentPane().add(opcion2);

		JTextArea opcion3 = new JTextArea("Listado con las 100 primeras personas universitarias y casadas, ordenadas de menor a mayor seg�n su edad.");
		opcion3.setForeground(Color.WHITE);
		opcion3.setOpaque(false);
		opcion3.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		opcion3.setBounds(12, 248, 306, 128);
		opcion3.setLineWrap(true);
		opcion3.setWrapStyleWord(true);
		getFrame().getContentPane().add(opcion3);

		JTextArea opcion4 = new JTextArea("Un listado con los 5 nombres m�s comunes entre los hinchas de River.");
		opcion4.setForeground(Color.WHITE);
		opcion4.setOpaque(false);
		opcion4.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		opcion4.setBounds(12, 340, 290, 128);
		opcion4.setLineWrap(true);
		opcion4.setWrapStyleWord(true);
		getFrame().getContentPane().add(opcion4);

		JTextArea opcion5 = new JTextArea("Listado de equipos seg�n la edad Mayor de sus socios.");
		opcion5.setForeground(Color.WHITE);
		opcion5.setOpaque(false);
		opcion5.setFont(new Font("Segoe UI Black", Font.BOLD, 14));
		opcion5.setBounds(12, 400, 290, 128);
		opcion5.setLineWrap(true);
		opcion5.setWrapStyleWord(true);
		getFrame().getContentPane().add(opcion5);
	}

	private void crearFondo() {
		JLabel fondo = new JLabel();
		fondo.setBounds(0, 0, 500, 470);
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/resources/fondo.jpg")));
		getFrame().getContentPane().add(fondo);
	}

	public void show(){
		this.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.getFrame().addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(
						null, "�Deseas salir?", 
						"Confirmaci�n", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					System.exit(0);
				}
			}
		});
		this.getFrame().setVisible(true);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
