package utilitarios;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import modelo.Socio;
public class LecturaCSV {
	private BufferedReader bufferLectura;

	public LecturaCSV(){
		this.bufferLectura=null;
		try{
			bufferLectura = new BufferedReader(new FileReader("src/resources/socios.csv"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Socio> obtenerSocios() throws IOException{
		ArrayList<Socio> listadoSocios= new ArrayList<Socio>();
		String SEPARADOR=";";
		String linea = bufferLectura.readLine();

		while (linea != null) {
			String[] datos = linea.split(SEPARADOR); 
			//System.out.println(datos[0]);
			//System.out.println(Arrays.toString(datos));
			listadoSocios.add(obtenerSocio(datos));
			linea = bufferLectura.readLine();
		}

		if (bufferLectura != null) {
			try {
				bufferLectura.close();
			} 
			catch (IOException e) {
				e.printStackTrace();

			}
		}
		return listadoSocios;
	}

	private Socio obtenerSocio(String[]datosSocio){
		Socio socio= new Socio(datosSocio[0],Integer.parseInt(datosSocio[1]),datosSocio[2],datosSocio[3],datosSocio[4]);
		return socio;
	}
}

		
