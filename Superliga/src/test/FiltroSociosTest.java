package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.OptionalDouble;
import java.util.OptionalInt;

import org.junit.Before;
import org.junit.Test;

import modelo.Equipo;
import modelo.FiltroSocios;
import modelo.Socio;

public class FiltroSociosTest {
	private ArrayList <Socio> socios;
	private FiltroSocios filtro;

	@Before
	public void inicializar() {
		socios= new ArrayList<Socio>();
		socios.add(new Socio("Agust�n",	39,	"San Lorenzo",	"Soltero",	"Terciario"));
		socios.add(new Socio("Anibal", 22, "Velez", "Casado", "Universitario"));
		socios.add(new Socio("Nicolas",	24,	"Hurac�n",	"Soltero",	"Secundario"));
		socios.add(new Socio("Mariano",	44,	"Velez",	"Soltero",	"Terciario"));
		socios.add(new Socio("Juan", 43, "San Lorenzo", "Casado", "Terciario"));
		socios.add(new Socio("Agust�n",	48,	"San Lorenzo",	"Soltero",	"Universitario"));
		socios.add(new Socio("Luis",	67,	"Gimnasia LP",	"Soltero",	"Secundario"));
		socios.add(new Socio("Mat�as",	22,	"Velez",	"Soltero",	"Secundario"));
		socios.add(new Socio("Enrique",	67,	"Estudiantes",	"Soltero",	"Terciario"));
		socios.add(new Socio("Mauro",	66,	"Hurac�n",	"Casado",	"Secundario"));
		socios.add(new Socio("Mauro",	40,	"Boca",	"Casado",	"Terciario"));
		socios.add(new Socio("Daniel",	38,	"River",	"Soltero",	"Terciario"));
		socios.add(new Socio("Luis",	69,	"River",	"Soltero",	"Secundario"));
		socios.add(new Socio("Nicol�s",	68,	"River",	"Soltero",	"Terciario"));
		socios.add(new Socio("Nicol�s",	33,	"River",	"Casado",	"Terciario"));
		socios.add(new Socio("Mauro",	31,	"Racing",	"Casado",	"Secundario"));
		socios.add(new Socio("Federico",63,	"Racing",	"Soltero",	"Terciario"));
		filtro= new FiltroSocios(socios);
	}

	@Test
	public void cantidadTotalSociosTest() {
		assertEquals(17,filtro.cantidadTotalSocios());
	}

	@Test
	public void promedioEdadSocios(){
		String club= "Racing";
		assertEquals(OptionalDouble.of(47.0),filtro.promedioEdadSocios(club));
	}

	@Test
	public void universitariosCasados(){
		ArrayList<Socio>universitariosCasados= new ArrayList<Socio>();
		universitariosCasados.add(new Socio("Anibal", 22, "Velez", "Casado", "Universitario"));
		assertArrayEquals(universitariosCasados.toArray(), filtro.filtrarCienPersonas("Universitario","Casado").toArray());
	}

	@Test
	public void nombresMasComunes(){
		ArrayList<String>nombresMasComunes= new ArrayList<String>();
		nombresMasComunes.add("Nicol�s");
		nombresMasComunes.add("Luis");
		nombresMasComunes.add("Daniel");
		assertArrayEquals(nombresMasComunes.toArray(),filtro.nombresMasComunes("River",5).toArray());
	}

	@Test
	public void ordenarClubesPorSocios(){
		ArrayList<String> clubesOrdenados= new ArrayList<String>();
		clubesOrdenados.add("River");
		clubesOrdenados.add("Velez");
		clubesOrdenados.add("San Lorenzo");
		clubesOrdenados.add("Racing");
		clubesOrdenados.add("Hurac�n");
		clubesOrdenados.add("Boca");
		clubesOrdenados.add("Estudiantes");
		clubesOrdenados.add("Gimnasia LP");
		assertArrayEquals(clubesOrdenados.toArray(),filtro.ordenarCantSocios().toArray());
	}

	@Test
	public void buscarMenorEdad(){
		assertEquals(OptionalInt.of(33),filtro.buscarMenorEdad("River"));
	}

	@Test
	public void buscarMayorEdad(){
		System.out.print(filtro.buscarMayorEdad("San Lorenzo").getAsInt());
		assertEquals(OptionalInt.of(48),filtro.buscarMayorEdad("San Lorenzo"));
	}

	@Test
	public void asignarEquipos(){
		ArrayList<Equipo> clubesConDatos= new ArrayList<Equipo>();
		clubesConDatos.add(new Equipo("River",filtro.promedioEdadSocios("River").getAsDouble(),filtro.buscarMayorEdad("River").getAsInt(),filtro.buscarMenorEdad("River").getAsInt()));
		clubesConDatos.add(new Equipo("Velez",filtro.promedioEdadSocios("Velez").getAsDouble(),filtro.buscarMayorEdad("Velez").getAsInt(),filtro.buscarMenorEdad("Velez").getAsInt()));
		clubesConDatos.add(new Equipo("San Lorenzo",filtro.promedioEdadSocios("San Lorenzo").getAsDouble(),filtro.buscarMayorEdad("San Lorenzo").getAsInt(), filtro.buscarMenorEdad("San Lorenzo").getAsInt()));
		clubesConDatos.add(new Equipo("Racing", filtro.promedioEdadSocios("Racing").getAsDouble(),filtro.buscarMayorEdad("Racing").getAsInt(), filtro.buscarMenorEdad("Racing").getAsInt()));
		clubesConDatos.add(new Equipo("Hurac�n", filtro.promedioEdadSocios("Hurac�n").getAsDouble(),filtro.buscarMayorEdad("Hurac�n").getAsInt(),filtro.buscarMenorEdad("Hurac�n").getAsInt()));
		clubesConDatos.add(new Equipo("Boca", filtro.promedioEdadSocios("Boca").getAsDouble(),filtro.buscarMayorEdad("Boca").getAsInt(), filtro.buscarMenorEdad("Boca").getAsInt()));
		clubesConDatos.add(new Equipo("Estudiantes", filtro.promedioEdadSocios("Estudiantes").getAsDouble(),filtro.buscarMayorEdad("Estudiantes").getAsInt(),filtro.buscarMenorEdad("Estudiantes").getAsInt()));
		clubesConDatos.add(new Equipo("Gimnasia LP", filtro.promedioEdadSocios("Gimnasia LP").getAsDouble(),filtro.buscarMayorEdad("Gimnasia LP").getAsInt(), filtro.buscarMenorEdad("Gimnasia LP").getAsInt()));
		assertArrayEquals(clubesConDatos.toArray(),filtro.asignarEquipos().toArray());
	}

	@Test
	public void cantidadDePersonasEquipo(){
		assertEquals(3,filtro.cantidadDeSocios("Velez"));
	}
}
